import React, { Component } from 'react';
import styles from './App.scss';

class App extends Component {
    render() {
        return (
            <div className={styles.App}>
                <h3>You're all set!</h3>
            </div>
        );
    }
}

export default App;
